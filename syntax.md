# hexer syntax

## comments

`# asdf`

:   commented out text

### human readable data

`# <........>`

:   human readable (ascii) version of read data (like `hexdump -C`)

:   . = non-ascii char

## file position

`01ab:`

:   current data file position (must match)

`:`

:   any file pos (will be replaced by current file pos)

### skipping

`---`<br />`1234:`

:   skip to 0x1234

`*---`<br />`1234:`

:   same but skip token gets replaced by skipped data (**not implemented**)

`+100:`

:   relative skip `0x100` bytes

## whitespace

`af LL`

:   whitespace is passed along intact

## defining data

### endianness (byte order)

see [wikipedia article](https://en.wikipedia.org/wiki/Endianness)

based on [struct syntax](https://docs.python.org/3/library/struct.html#byte-order-size-and-alignment)

`@@`

:   set to native (default)

`@<`

:   set to little-endian

`@>`

:   set to big-endian

### hex data

`af09`

:   outputted hex data

:   can be verified or used to change data in file (**not implemented**)

`.`

:   any data, will be replaced with actual data in output

`=abacadaba1`

:   exactly match read data (ie check magic header)

### data types

see [wikipedia](https://en.wikipedia.org/wiki/Data_type)

based on [struct syntax](https://docs.python.org/3/library/struct.html#format-characters)

#### integer

see [wikipedia](https://en.wikipedia.org/wiki/Integer_(computer_science))

`LHBI`

: read data of type **L**ong,s**H**ort,**B**yte,**I**nt (unsigned)

`-L-H`

: read data of type **L**ong,s**H**ort (signed)

#### float

see [wikipedia](https://en.wikipedia.org/wiki/Floating-point_arithmetic)

`FDE`

: read data of type **F**loat,**D**ouble,half float (**E**)

#### bool

see [wikipedia](https://en.wikipedia.org/wiki/Boolean_data_type)

`O`

: read data of type b**O**ol (padded to nearest byte)


#### string

see [wikipedia](https://en.wikipedia.org/wiki/String_(computer_science))

`S*10`

:   ascii string with 10 chars

`U*10`

:   unicode string with 10 chars (**not implemented**)

`S0`

:   zero- or null-terminated string (**not implemented**)

`S"..."`

:   simple string, size based on chars between `"` (**not implemented**)


### data types output

`L[123]`

:   long w/ data

`S*3"..."`

:   3 char string

`S*10".....`<br />`...."`

: multi line 10 char string


`S*10".....`<br />`...\""`

:   multi line 10 char string with last `"` char escaped

`S0"..."`

:   zero-terminated string (**not implemented**)

`U"häxa"`

:   simple unicode string (**not implemented**)

## macros

`<=name>...</name>`

:   create macro as name

`<name />`

:   run name macro

`<name>...</name>`

:   output from previous

### namespaces

`<name ns />`

:   run macro under ns namespace (var names get ns. prefixed)

### default macro

`<=default></default>`

:   set default macro (used after eof or new hx file)

## repeating

`L*4`

:   read 4 long

`L[1]L[2]L[3]L[4]`

:   4 longs w/ data

`..*4`<br />`*4`

:   4 x 4 bitmap (keeps whitespace before `*`)

`S*2*2`

:   2 strings w/ 2 chars


`<name />*3`

:   repeat macro 3 times

## variables

`L{var}`

:   store long as var

`LL{var,var2}`

:   store two longs as var and var2

`LL{var:123,var2:321}`

:   output for previous with data

`H{var.var2}`

:   namespaced var (see [macro namespaces](#namespaces))

:   dots, as well as some other special chars are allowed in var names

`S{var}""`

:   string variable

### interpolation

see [wikipedia](https://en.wikipedia.org/wiki/String_interpolation)

`LL*{var}`

:   repeat two longs using var (see [repeating](#repeating))

`<name />*{var}`

:   repeat macro using var

`<vertex_{corner}/>`

:   run macro `vertex_{corner}` (ex `vertex_top_left`)

`<name ns{var}/>`

:   run macro under `ns{var}` namespace (ex `ns0`, `ns10`)

`S*{var}`

:   fixed string length using var

`LL{var,var2}*4[{var:123,var2:321},...]`

:   4 dicts with data (**not implemented**)

`..*{width}`<br />`*{height}`

:   variable sized bitmap

`B*{var{a}}B*{var{b}}`

:   nested interpolation (**not implemented**)

`BB{a,b}`<br />`<=v1.0>III</v1.0>`<br />`<=v1.1>IIII</v1.0>`<br />`<v{a}.{b}/>`

:   conditional macro running, enabling versioned headers

`+{var}:`

:   relative skip using var

`---`<br />`{var}:`

:   skip to filepos using var

### special vars

#### i

current repeat iteration nr

`<name ns_{i} />*3`

:   repeat macro 3 times under ns_{i} namespace
:   {i} gets replaced with i var (ns_0 ... ns_2)


## importing

(**not implemented**)

`<+filename/>`

:   import filename and run as macro

:   this (short macro syntax) **will not** include output from parser

`<+filename></+>`

:   this (long macro syntax) **will** include output from parser

`<+filename ns></+>`

:   import filename and run as macro under ns namespace (with parser output)

`<+"filename with space" ns/>`

:   import "filename with space" and run as macro under ns namespace (without parser output)
