```
read file and output hex

[zero-padded-hex-filepos(based on file-size)]: [hex-data] # [comments]
hex-data:
    a-z0-9  outputted hex data, can be verified or used to change data in file
    .       any data, will be replaced with actual data in output
    =abc123 hex data has to match read data (ie check magic header)
    @>      set endianess (as in struct, @@|@>|@<)
    LHBI    read data of type Long,sHort,Byte,Int (unsigned)
    -L-H    read data of type Long,sHort (signed)
    FDE     read data of type Float,Double,half float
    O       read data of type bOOl (padded to nearest byte)
    L*4     read 4 long
    L[123]  long w/ data
    L*4[1,2,3,4]
        4 longs w/ data
    L{name} named type
    LL{name,name2}
        multiple named types (dicts)
    LL{name:123,name2:321}
        dicts with data
    LL{name,name2}*4[{name:123,name2:321},...]
        4 dicts with data
    LL*{value}
        repeat types based on value
    H{name.name2}
        "namespaced" name. dots, as well as some other special chars are allowed
    "..."   string (can span rows until end " is found, \" escapes it)
    S"..\x00" zero-terminated string w/ data
    S{name}""
        named string
    S*10    10 char fixed string
    S*{value}
        fixed string with length based on value
    U       same as string, but unicode
    af LL   spaces are ignored
    <=name>...</name>
        create macro
    <name />
        use macro
    <name ns />
        use macro in ns namespace (var names get ns. prefixed)
    <name />*3
        repeat macro 3 times
    <name ns_{i} />*3
        repeat macro 3 times in ns_{i} (ns_0 ... ns_n) namespace
    <name />*{value}
        repeat macro based on value
    <name>...</name>
        used macro w/ data output
    <=default></default>
        set default macro (used after eof)
    <+filename/>
        import filename and run as macro
    <+filename ns></filename>
        import filename and run as macro with ns namespace
    <+"filename with space" ns></filename>
        import "filename with space" and run as macro with ns namespace
    ...
        used at start of line will skip until next filepos
    ...<
        same but gets replaced by skipped data as hex
    ...>
        same but gets inserted before next filepos instead
```